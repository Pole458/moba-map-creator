# MOBA Map Creator #

![Image of Screenshot](Screenshot.PNG)

The map creator allows to create a map starting from a tileset. The map is saved as a binary file and contains informations about tiles position and crossable terrain.


Here you can find the [server](https://bitbucket.org/Pole458/moba-server/overview) and the [client](https://bitbucket.org/Pole458/moba-client/overview).


You can also find a video trailer here:

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/KT5oj_REpf0/0.jpg)](http://www.youtube.com/watch?v=KT5oj_REpf0)

## Credits ##

Made by Pole.