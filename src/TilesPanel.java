import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class TilesPanel extends JPanel implements ActionListener, MouseListener, MouseWheelListener {

	public int rollY;
	public static short cellSelected;
	public static short layerSelected;
	public JComboBox<String> layers;
	
	public TilesPanel() {
		rollY=24;
		cellSelected=0;
		setPreferredSize(new Dimension(384,1104));
		addMouseListener(this);
		addMouseWheelListener(this);
		setLayout(null);
		layers = new JComboBox<String>(new String[]{"Floor 0","Flowers 1","Walls 2","Events 3"});
		add(layers);
		layers.addActionListener(this);
		layers.setBounds(144, 0, 100, 24);
		
	}
	
	public void paint(Graphics g) {
		g.clearRect(0,24,384,getHeight());
		g.drawImage(new ImageIcon("TileSet1.png").getImage(), 0, rollY,null);
		g.setColor(Color.RED);
		g.drawRect(cellSelected%16*24,cellSelected/16*24+rollY,24,24);
	
		g.clearRect(0,0,384,24);
		paintComponents(g);
		g.drawImage(new ImageIcon("TileSet/tile_"+cellSelected+".png").getImage(),120,0,null);
		g.drawRect(120,0,24,24);
		g.setColor(Color.BLACK);
		g.drawRect(0,0,384,24);
		g.drawString("Cell selected: "+cellSelected, 12, 20);
	}

	public static short getSelectedCell() {
		return cellSelected;
	}
	public static int getSelectedLayer() {
		return layerSelected;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		cellSelected=(short)(e.getX()/24+((e.getY()-rollY)/24*16));
		cellSelected=(short)Math.min(cellSelected, 703);
		repaint();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==layers) {
			layerSelected=(short)layers.getSelectedIndex();
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		rollY-=e.getWheelRotation()*24;
		if(rollY<MapFrame.H-1104) rollY=MapFrame.H-1104;
		if(rollY>24) rollY=24;
		repaint();
	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
