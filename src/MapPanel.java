import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class MapPanel extends JPanel implements ActionListener, MouseListener, MouseMotionListener, KeyListener {

	public int tileX;
	public int tileY;
	public short[][][] tiles;
	public JTextField xTF;
	public JTextField yTF;
	public JButton applyButton;
	public JCheckBox[] layers;
	public JButton saveButton;
	public JLabel positionLabel;
	
	public boolean ctrlPressed;
	public boolean altPressed;
	
	public int drawX;
	public int drawY;
	
	public MapPanel() {
		
		xTF = new JTextField("10");
        yTF = new JTextField("10");
        add(xTF);
        add(yTF);
        xTF.setBounds(0,0,50,24);
        yTF.setBounds(50,0,50,24);
		
        applyButton= new JButton("Apply");
        add(applyButton);
        applyButton.setBounds(100,0,75,24);
        applyButton.addActionListener(this);
        
        layers = new JCheckBox[4];
        layers[0] = new JCheckBox("L 0",true);
        layers[1] = new JCheckBox("L 1",true);
        layers[2] = new JCheckBox("L 2",true);
        layers[3] = new JCheckBox("L 3",true);
        add(layers[0]);
        add(layers[1]);
        add(layers[2]);
        add(layers[3]);
        layers[0].setBounds(175, 0, 50, 24);
        layers[1].setBounds(225, 0, 50, 24);
        layers[2].setBounds(275, 0, 50, 24);
        layers[3].setBounds(325, 0, 50, 24);
        layers[0].addActionListener(this);
        layers[1].addActionListener(this);
        layers[2].addActionListener(this);
        layers[3].addActionListener(this);
        
        saveButton= new JButton("save");
        saveButton.setBounds(375,0,75,24);
        saveButton.addActionListener(this);
        add(saveButton);
        
        positionLabel = new JLabel("0,0");
        add(positionLabel);
        positionLabel.setBounds(450,0,50,24);
        
        addMouseMotionListener(this);
        addMouseListener(this);
        addKeyListener(this);
        
		setLayout(null);
		
		tileX=Integer.valueOf(xTF.getText());
		tileY=Integer.valueOf(yTF.getText());
		tiles = new short[4][tileX][tileY];
		setBounds(384,0,Math.max(500,(tileX+1)*24),48+tileY*24);
		
		for(int i=0; i<tileX; i++)
			for(int j=0; j<tileY; j++)
				for(int l=0; l<4; l++)
					tiles[l][i][j]=100;
		
		try {
			ObjectInputStream stream = new ObjectInputStream(new FileInputStream("map.bin"));
			tiles = (short[][][])stream.readObject();
			stream.close();
			tileX=0;
			tileY=0;
			while(true)
				try {
					if(tiles[0][tileX][0]<1000)	tileX++;
				}
				catch(ArrayIndexOutOfBoundsException e) {
					break;
				}
			while(true)
				try {
					if(tiles[0][0][tileY]<1000) tileY++;
				}
				catch(ArrayIndexOutOfBoundsException e) {
					break;
				}
			
			setBounds(384,0,Math.max(450,(tileX+1)*24),48+tileY*24);
			xTF.setText(String.valueOf(tileX));
			yTF.setText(String.valueOf(tileY));
			repaint();
		} catch(ClassNotFoundException exception) {	
		} catch (IOException e) {
		}
		
		drawX=0;
		drawY=0;
		
		ctrlPressed=false;
		altPressed=false;
	}
	
	public void paint(Graphics g) {

		g.clearRect(0, 0, getWidth(), getHeight());
		
		try {
			int selX=(int)getMousePosition().getX()/24+drawX;
			int selY=((int)getMousePosition().getY()-24)/24+drawY;
			positionLabel.setText(selX + ","+ selY );
		}
		catch(NullPointerException e){};
		
		for(int i=drawX; i<Math.min(tileX,(MapFrame.W-384)/24+drawX); i++)
			for(int j=drawY; j<Math.min(tileY,(MapFrame.H-24)/24+drawY); j++)
				for(int l=0; l<4; l++)
					if(layers[l].isSelected())
						g.drawImage(new ImageIcon("TileSet/tile_"+tiles[l][i][j]+".png").getImage(),(i-drawX)*24,24+(j-drawY)*24,null);

		g.setColor(Color.RED);
		g.drawRect(0-drawX*24, 24-drawY*24, tileX*24, tileY*24);
		
		paintComponents(g);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource()==applyButton) {
			int oldTileX=tileX;
			int oldTileY=tileY;
			short[][][] oldTiles = tiles;
			
			tileX=Integer.valueOf(xTF.getText());
			tileY=Integer.valueOf(yTF.getText());
			
			tiles = new short[4][tileX][tileY];
			for(int i=0; i<tileX; i++)
				for(int j=0; j<tileY; j++)
					for(int l=0; l<4; l++)
						if(i<oldTileX && j<oldTileY)
							tiles[l][i][j]=oldTiles[l][i][j];
						else tiles[l][i][j]=100;
			
			setBounds(384,0,Math.max(425,tileX*24+24),48+tileY*24);
			
		}
		
		if(e.getSource()==saveButton) {
			try {
				ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("map.bin"));
				stream.writeObject(tiles);
				stream.close();
			} catch(IOException exception) {
			}
		}
		
		repaint();
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getSource().equals(this)) {
			if(altPressed) {
				int selX=e.getX()/24+drawX;
				int selY=(e.getY()-24)/24+drawY;
				if(selX>=0 && selX<tileX && selY>=0 && selY<tileY) tiles[TilesPanel.layerSelected][selX][selY]=100;
				repaint();
			} 
			else if(!ctrlPressed) {
					int selX=e.getX()/24+drawX;
					int selY=(e.getY()-24)/24+drawY;
					if(selX>=0 && selX<tileX && selY>=0 && selY<tileY) tiles[TilesPanel.layerSelected][selX][selY]=TilesPanel.getSelectedCell();
					repaint();
				}
		}
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if(e.getSource().equals(this)) {
			if(altPressed) {
				int selX=e.getX()/24+drawX;
				int selY=(e.getY()-24)/24+drawY;
				if(selX>=0 && selX<tileX && selY>=0 && selY<tileY) tiles[TilesPanel.layerSelected][selX][selY]=100;
				repaint();
			}
			else if(ctrlPressed) {
					int selX=e.getX()/24+drawX;
					int selY=(e.getY()-24)/24+drawY;
					if(selX>=0 && selX<tileX && selY>=0 && selY<tileY) tiles[TilesPanel.layerSelected][selX][selY]=TilesPanel.getSelectedCell();
					repaint();
			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if(e.getSource().equals(this)) {
			int selX=e.getX()/24+drawX;
			int selY=(e.getY()-24)/24+drawY;
			positionLabel.setText(selX + ","+ selY );
		}
	}
	

	@Override
	public void mouseEntered(MouseEvent e) {
		grabFocus();
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_CONTROL) ctrlPressed=true;
		if(e.getKeyCode()==KeyEvent.VK_ALT) altPressed=true;
		
		if(e.getKeyCode()==KeyEvent.VK_W) {
			drawY=Math.max(0, drawY-1);
			repaint();
		}
		if(e.getKeyCode()==KeyEvent.VK_A) {
			drawX=Math.max(0, drawX-1);
			repaint();
		}
		if(e.getKeyCode()==KeyEvent.VK_D) {
			if(tileX>(MapFrame.W-384)/24) drawX=Math.min(drawX+1,tileX-(MapFrame.W-384)/24+1);
			repaint();
		}
		if(e.getKeyCode()==KeyEvent.VK_S) {
			if(tileY>(MapFrame.H-24)/24) drawY=Math.min(drawY+1,tileY-(MapFrame.H-36)/24+1);
			repaint();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_CONTROL) ctrlPressed=false;
		if(e.getKeyCode()==KeyEvent.VK_ALT) altPressed=false;
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
