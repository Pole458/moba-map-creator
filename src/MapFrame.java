import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class MapFrame extends JFrame implements ComponentListener {
	
	public TilesPanel tp;
	public MapPanel mp;
	public static int W;
	public static int H;
	
	public MapFrame(String s) {
		super(s);
		W=910;
		H=600;
		setLocation(50,50);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(true);
		addComponentListener(this);
		
		setMinimumSize(new Dimension(W,H));
        setVisible(true);
        setLayout(null);
        
        tp = new TilesPanel();
        add(tp);
        tp.setBounds(0,0,384,1104);
        
        mp = new MapPanel();
        add(mp);
        
       repaint();
	}
	
	
	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentResized(ComponentEvent e) {
		if(e.getSource()==this) {
			W=getWidth();
			H=getHeight();
			if(tp!=null)
				if(tp.rollY<MapFrame.H-1104) tp.rollY=MapFrame.H-1104;
		}
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub
	}
	
	
	public static void main(String args[]) {
		new MapFrame("Map Creator");
	}
}
